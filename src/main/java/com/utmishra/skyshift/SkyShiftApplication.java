package com.utmishra.skyshift;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkyShiftApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkyShiftApplication.class, args);
	}

}
